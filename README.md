# QL_based_Control

The repository contains sample programs for controlling systems using Q-Learning. The project uses [gym-tf](https://gitlab.com/major-project2/gym-tf) library for doing the same. 

## Dependencies

Inorder to run examples notebooks in this repositorty following modules needs to installed,

- OpenAI's gym
- Numpy
- Scipy
- Control
- Matplotlib

## Content

The repo consists of three notebooks as follows:

1. Setup_env.ipynb - The notebook should executed when the repo is first cloned to setup the environemnt required to the run these example notebooks.

2. Q-Learning_Ex_1.ipynb - The notebook contains a Non-Isothermal CSTR control problem which solved using Q-Learning.

3. Q-Learning_Ex_1.ipynb - The notebook contains a Heat Exchanger along with bypass stream and sensor dynamics which is solved using Q-Learning.

**Note:** _All the notebooks were observed to execute neatly in jupyter lab. However, trying to execute them is VS Code threw errors._
